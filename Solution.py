from __future__ import division
import numpy as np

class Solution:
    ''' collection of ions or polymers that constiute the solution around cylinders '''
    def __init__(self, cyl1, cyl2, chain, bead_spacing, lx, ly, lz):
        self.center_positions = []
        center1 = np.array(cyl1.center)
        center2 = np.array(cyl2.center)
        base1 = np.array([1, 0, 0])
        base2 = np.array([0, 1, 0])
        base3 = np.array([0, 0, 1])
        nx = int(lx / bead_spacing)
        ny = int(ly / bead_spacing)
        nz = int(lz / bead_spacing / chain.n_beads)
        forbiddenR2 = (cyl1.radius + cyl1.beadsize)**2
        for i in range(nx):
            for j in range(ny):
                for k in range(nz):
                    pos = (i*bead_spacing-0.5*lx) * base1 + (j*bead_spacing-0.5*ly) * base2 + (
                            (chain.n_beads*(k + 0.5))*bead_spacing-0.5*lz) * base3
                    dist1 = pos - center1
                    dist2 = pos - center2
                    if dist1[0]**2 + dist1[1]**2 < forbiddenR2 or dist2[0]**2 + dist2[1]**2 < forbiddenR2:
                        continue
                    else:
                        self.center_positions.append(pos)

        self.solution_n_centers = len(self.center_positions)
        self.solution_n_beads = self.solution_n_centers * chain.n_beads
        self.b_types = 'harmonic'
        self.num_of_bonds = self.solution_n_centers * (chain.n_beads-1)

