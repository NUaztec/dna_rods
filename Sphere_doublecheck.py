from __future__ import division
import os
import sys

sys.path.append('/projects/b1030/hoomd/hoomd-2.5.1/')
from hoomd import *
from hoomd.md import *
from hoomd import deprecated as hdepr
from Potential import soft_repulsive
from Potential import zerof
from sphere import SphericalTemplate
import numpy as np


try:
    job_id = int(os.environ['RSEED'])
    molarity = float(os.environ['MOLE'])
    n_dissociate = float(os.environ['Charge'])
except:
    job_id = 42
    molarity = 0.5
    print('parameter not found in environment, using default!')
# constants
path = 'results/'
num_avg = 1000
intv = 150
force_sample_num = intv * num_avg
HELIX = type('HELIX', (object,), {})()

HELIX.cyl_radius=3.5
HELIX.height = 12
HELIX.beadsize = 0.6
init_pos = HELIX.cyl_radius + 0.8 * HELIX.beadsize

sphere1 = SphericalTemplate(HELIX.cyl_radius, -init_pos)
sphere2 = SphericalTemplate(HELIX.cyl_radius, init_pos)

charge1 = sphere1.num_beads

HELIX.filename = 'testSphere_mole' + str(molarity) +'_charge'+str(n_dissociate)+ '_IBI_' + str(job_id)


def cback_fx0(tstep):
    f_sum = 0
    for ii in range(charge1):
        p = system.particles[ii]
        f_sum += p.net_force[0]
    return f_sum


def cback_fx1(tstep):
    f_sum = 0
    for ii in range(charge1):
        p = system.particles[ii + charge1]
        f_sum += p.net_force[0]
    return f_sum


def cback_dist(tstep):
    r = system.particles[charge1].position[0] - system.particles[0].position[0]
    return r


def cback_j(tstep):
    return j_slice

delta_r = 8.0
bly = HELIX.height + 2 * HELIX.cyl_radius + delta_r
blz = HELIX.height
blx = 4 * HELIX.cyl_radius + blz + 4 * delta_r

final_pos = init_pos + delta_r

stepsize = 0.05
radi_Na = 0.15
radi_Cl = 0.21
epsilon_dict = {"0.3": 92.2,
                "0.5": 85.7,
                "0.7": 80.3,
                "1.0": 73.5,
                "2.0": 60.1,
                "3.0": 48.85454,
                "4.0": 37.72745}

eps_salt = epsilon_dict.get(str(molarity), 92.2)
ion_charge = 7.49 / np.sqrt(eps_salt)  # system unit charge (per ion)

positions = np.arange(init_pos, final_pos + 0.00001, stepsize)

N_A = 6.022 / 10.0  # Avogadro's Constant reduced
vol = blx * bly * blz - 2 * np.pi * (HELIX.cyl_radius**2) * blz
num_ion = int(vol * molarity * N_A)  # positive or negative, only one copy!
num_counterion = int(round(2 * charge1 * n_dissociate))
num_slices = len(positions)

nvt_time = 4e5
timestep = 0.0005
num_particles = 2 * num_ion + num_counterion + 2 * charge1

print 'position from\t' + str(init_pos) + 'to\t' + str(final_pos)
print positions
print("number of particles: " + str(num_particles))

context.initialize()

snapshot = data.make_snapshot(N=num_particles, particle_types=['A', 'B', 'C', 'D'],
                              box=data.boxdim(Lx=blx, Ly=bly, Lz=blz))
snapshot.box = data.boxdim(Lx=blx, Ly=bly, Lz=blz, xy=0.0, xz=0.0, yz=0.0)  # elongated in x direction
system = init.read_snapshot(snapshot)

z_unit_vector = np.array([0, 0, 1])


for i in range(sphere1.num_beads):
    snapshot.particles.position[i] = sphere1.position[i]
    snapshot.particles.velocity[i] = (0, 0, 0)
    snapshot.particles.charge[i] = -1 * n_dissociate * ion_charge
    snapshot.particles.typeid[i] = 0
    snapshot.particles.diameter[i] = 1

for i in range(sphere2.num_beads):
    snapshot.particles.position[i+charge1] = sphere2.position[i]
    snapshot.particles.velocity[i+charge1] = (0, 0, 0)
    snapshot.particles.charge[i+charge1] = -1 * n_dissociate * ion_charge
    snapshot.particles.typeid[i+charge1] = 0
    snapshot.particles.diameter[i+charge1] = 1

idx = 2 * charge1  # start from the 2nd particle, which is the first ion
delta_r = 0.6
list_ions = []
overlapCount = 0
ionCount = 0
clCount = 0
anionCount = 0
d2 = radi_Cl ** 2
n_dna = 2 * charge1
forbiddenR1 = np.square(HELIX.cyl_radius + 0.5* sphere1.beadsize + radi_Cl + delta_r)
print 'Add ions'
while True:
    overlap = False
    x = blx * 0.5 * (2.0 * np.random.random() - 1.0)
    y = bly * 0.5 * (2.0 * np.random.random() - 1.0)
    z = blz * 0.5 * (2.0 * np.random.random() - 1.0)
    dist_0 = np.square(x + init_pos) + np.square(y)
    dist_1 = np.square(x - init_pos) + np.square(y)

    if dist_0 < forbiddenR1 or dist_1 < forbiddenR1:
        overlap = True
        overlapCount += 1
        continue
    elif len(list_ions) > 0:  # check if overlap with other ions

        for pos_ions in list_ions:
            x0 = pos_ions[0] - x
            y0 = pos_ions[1] - y
            z0 = pos_ions[2] - z

            if x0 * x0 + y0 * y0 + z0 * z0 < d2:
                overlap = True
                break
            else:
                overlap = False
                # print 'ion' + str(idx)

    # add particle
    if not overlap:
        list_ions.append([x, y, z])
        snapshot.particles.position[idx] = (x, y, z)

        # assign charge  # if ion size were different, this has to be moved before comparing for judgement
        if idx < num_ion + n_dna:
            snapshot.particles.charge[idx] = ion_charge
            snapshot.particles.typeid[idx] = 2  # type C
            snapshot.particles.diameter[idx] = 2 * radi_Na
            snapshot.particles.velocity[idx] = np.random.normal(size=3)
            ionCount += 1
        elif idx < 2 * num_ion + n_dna:
            snapshot.particles.charge[idx] = -ion_charge  # anions and additional ions added
            snapshot.particles.typeid[idx] = 3  # type D
            snapshot.particles.diameter[idx] = 2 * radi_Cl
            snapshot.particles.velocity[idx] = np.random.normal(size=3)
            snapshot.particles.mass[idx] = 35 / 23.5
            clCount += 1
        else:
            snapshot.particles.charge[idx] = ion_charge  # anions and additional ions added
            snapshot.particles.typeid[idx] = 2  # type C
            snapshot.particles.diameter[idx] = 2 * radi_Na
            snapshot.particles.velocity[idx] = np.random.normal(size=3)
            snapshot.particles.mass[idx] = 1  # mass is different
            anionCount += 1

        idx += 1
    if idx == num_particles:
        print 'ions added, overlap = ' + str(overlapCount)
        break

system.restore_snapshot(snapshot)

#xml = hdepr.dump.xml(group=group.all(), filename=HELIX.filename + '.xml', vis=True)
#dump.gsd(HELIX.filename + "-initial.gsd", group=group.all(), overwrite=True, period=None)

# =================== Force Field ================================ #
sig = 1.0
epsi = 1.0

delta_Na = radi_Na + HELIX.cyl_radius
delta_Cl = radi_Cl + HELIX.cyl_radius

rcut2 = 2**(1/6.0)
potentialFilename = {"0.3": '03',
                     "0.5": '05',
                     "0.7": '07',
                     "1.0": '1',
                     "2.0": '2',
                     "3.0": '3',
                     "4.0": '3'}

nl = nlist.cell()

table = pair.table(width=650, nlist=nl)
table.pair_coeff.set(['A', 'B'], ['A', 'B'], func=soft_repulsive, rmin=0.01, rmax=2.5,
                     coeff=dict(epsilon=epsi, sigma=sphere1.beadsize))
table.pair_coeff.set(['A', 'B'], 'C', func=soft_repulsive, rmin=0.01, rmax=2.5,
                     coeff=dict(epsilon=epsi, sigma=(0.5*sphere1.beadsize+radi_Na)))
table.pair_coeff.set(['A', 'B'], 'D', func=soft_repulsive, rmin=0.01, rmax=2.5,
                     coeff=dict(epsilon=epsi, sigma=(0.5*sphere1.beadsize+radi_Cl)))
table.set_from_file('C', 'C', filename=('potn_NaNa-'+potentialFilename.get(str(molarity),'03')+'m.dat'))
table.set_from_file('C', 'D', filename=('potn_NaCl-'+potentialFilename.get(str(molarity),'03')+'m.dat'))
table.set_from_file('D', 'D', filename=('potn_ClCl-'+potentialFilename.get(str(molarity),'03')+'m.dat'))

typeA = group.type(name='a-particles', type='A')
typeB = group.type(name='b-particles', type='B')
typeC = group.type(name='c-particles', type='C')
typeD = group.type(name='d-particles', type='D')
typeIons = group.union(name='ions', a=typeC, b=typeD)

charged = group.charged()  # pppm section
nl = nlist.cell()
#pppm = charge.pppm(group=charged, nlist=nl)
#pppm.set_params(Nx=128, Ny=64, Nz=64, order=4, rcut=2.0)

integrate.mode_standard(dt=timestep)
Qlog = ['temperature', 'potential_energy', 'pair_table_energy']
logger = analyze.log(quantities=Qlog, period=100, filename=(HELIX.filename+".log"), overwrite=True)

integrator = integrate.nve(typeIons, limit=0.05)
zero = update.zero_momentum(period=100)
# dcd = dump.dcd(filename="traj{}.dcd".format(job_id), period=5e4, overwrite=True)
dumper = dump.gsd(HELIX.filename + ".gsd",
                  period=20000,
                  group=group.all(),
                  overwrite=True)

run(3000)
integrator.disable()
zero.disable()
integrate.mode_standard(dt=timestep)
integrator = integrate.langevin(group=typeIons, kT=1.0, seed=job_id, dscale=0.2)
# integrator.set_gamma('C', gamma=0.05)
# integrator.set_gamma('D', gamma=0.05)
run(nvt_time)
logger.disable()

Qlog.extend(['dist_idx', 'dist', 'av_fx0', 'av_fx1'])
logger = analyze.log(quantities=Qlog, period=intv, filename=HELIX.filename+".log", overwrite=True)
logger.register_callback('av_fx0', cback_fx0)
logger.register_callback('av_fx1', cback_fx1)
logger.register_callback('dist', cback_dist)
logger.register_callback('dist_idx', cback_j)
moves = 100
dr = stepsize / moves * np.array([1, 0, 0])

j_slice = 0
run(force_sample_num)
for j_slice in range(1, num_slices-1):
    # move nano particle
    logger.disable()
    for k in range(moves):

        for i in range(charge1):
            pos0 = np.array(system.particles[i].position)
            pos1 = np.array(system.particles[i + charge1].position)
            (x0, y0, z0) = pos0 - dr
            (x1, y1, z1) = pos1 + dr
            system.particles[i].position = (x0, y0, z0)
            system.particles[i + charge1].position = (x1, y1, z1)


        run(200)
    # move
    #  finished
    run(5e4)  # to let it equilibriate
    logger.enable()

    run(force_sample_num)

integrator.disable()

xml2 = hdepr.dump.xml(group=group.all(), filename="final{}.xml".format(job_id), vis=True)
# data process
data = np.genfromtxt(HELIX.filename+".log", delimiter='\t')
size = len(data)
points = int(size / num_avg)

j = 1
for i in range(points):
    f1 = 0
    f2 = 0
    u_wall = 0
    if j < size - 2:
        dist = data[j, 5]
        while data[j, 4] == i:
            f1 += data[j, 6]
            f2 += data[j, 7]
            j += 1
            if j == size - 1:
                break
    f1 /= num_avg
    f2 /= num_avg
    with open(path+HELIX.filename+'_npForce.dat', 'a') as f:
        f.write(str(dist) + '\t' + str(f1) + '\t' + str(f2) +'\t'+str(u_wall)+ '\n')
