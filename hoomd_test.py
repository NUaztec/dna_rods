from __future__ import division
import os
import sys

sys.path.append('/projects/b1030/hoomd/hoomd-2.3.0/')
from hoomd import *
from hoomd.md import *
from hoomd import deprecated as hdepr
from Potential import soft_repulsive
from Potential import zerof
import numpy as np

charge1 = 10


def cback_fx0(tstep):
    f_sum = 0
    for i in range(10):
        p = system.particles[i]
        f_sum += p.net_force[0]
    return f_sum


def cback_fx1(tstep):
    f_sum = 0
    for i in range(10):
        p = system.particles[i + 10]
        f_sum += p.net_force[0]
    return f_sum


def cback_dist(tstep):
    r = system.particles[0].position[0] - system.particles[charge1].position[0]
    return r


def cbadk_j(tstep):
    return j


try:
    job_id = int(os.environ['RSEED'])
    molarity = float(os.environ['MOLE'])
except:
    job_id = 42
    molarity = 0.5
    print('parameter not found in environment, using default!')
# constants
num_avg = 1000
intv = 150
force_sample_num = intv * num_avg

HELIX = type('HELIX', (object,), {})()
HELIX.pitch = 1.0
HELIX.filename = 'screen' + str(job_id) + '_mole_' + str(molarity)
blz = HELIX.pitch * 10

HELIX.diameter = 2.0
blx = 20
bly = 16
init_pos = HELIX.diameter / 2.0
final_pos = init_pos + 3.0

stepsize = 0.05
radi_Na = 0.15
radi_Cl = 0.21
molarity = 1
epsilon_dict = {"0.5": 85.7,
                "0.7": 80.3,
                "1": 73.5}

eps = epsilon_dict[str(molarity)]
print eps
