import numpy as np
import matplotlib.pyplot as plt


def shift_lj(r, rmin, rmax, epsilon, sigma, delta, shift):
    ri2 = (sigma / (r - delta)) * (sigma / (r - delta))
    sig_r_delta6 = ri2 * ri2 * ri2
    potential = 4 * epsilon * (sig_r_delta6 * sig_r_delta6 - sig_r_delta6) - shift
    f = 4 * epsilon / r * (12 * sig_r_delta6 * (sig_r_delta6 - 0.5))
    return (potential, f)

def lj(r, rmin, rmax, epsilon, sigma):
    ri2 = sigma / r * sigma / r
    sig_r_delta6 = ri2 * ri2 * ri2
    shift = 4* epsilon * ((sigma/rmax)**12 - (sigma/rmax)**6)
    potential = 4 * epsilon * (sig_r_delta6 * sig_r_delta6 - sig_r_delta6) - shift
    f = 4 * epsilon / r * (12 * sig_r_delta6 * (sig_r_delta6 - 0.5))
    return (potential, f)


def zerof(r, rmin, rmax, epsilon, sigma):
    return (0, 0)


def pot_shift(rc, epsilon, sigma, delta):
    sig_r3 = (sigma / (rc - delta)) * (sigma / (rc - delta)) * (sigma / (rc - delta))
    sig_r_delta6 = sig_r3 * sig_r3
    vrc = 4 * epsilon * sig_r_delta6 * (sig_r_delta6 - 1)
    return vrc


def soft_repulsive(r,rmin, rmax, sigma, epsilon):

    V = epsilon * ((sigma / r) ** 12 - 0.5 ** 12)
    F = epsilon/sigma * 12 * (sigma/r) ** 13
    return (V, F)


class PotentialTest(object):
    def __init__(self, param):
        self.diameter = 0.425
        self.plot_range = self.diameter*2**(1/6.0)
        self.x = np.arange(0.5*self.diameter, self.plot_range, 0.002)
        self.param = param

    def plot(self, funct):
        vf = funct(self.x, 0.5*self.diameter, self.plot_range, 1.0, self.diameter)
        y = vf[0]
        plt.plot(self.x, y)
        plt.plot(self.x, vf[1])

        plt.ylim(-3, 12)
        plt.show()

test = PotentialTest(1.0)
test.plot(lj)
