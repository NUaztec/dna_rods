#!/bin/bash
#SBATCH --job-name="cylinder-salt"
#SBATCH -A b1030
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --ntasks-per-node=1
#SBATCH -t 100:00:00
#SBATCH -p buyin
#SBATCH --gres=gpu:p100:1

module purge all
module load cuda/cuda_8.0.61_mpich
module load python


cd $SLURM_SUBMIT_DIR

export RSEED=4
export MOLE=2.0
export Charge=1.0
mpirun -n 1 python Cyl_Wall.py
