from __future__ import division
import os
import sys

sys.path.append('/projects/b1030/hoomd/hoomd-2.3.0/')
from hoomd import *
from hoomd.md import *
from hoomd import deprecated as hdepr
from Potential import soft_repulsive
from Potential import zerof
import numpy as np

charge1 = 10

try:
    job_id = int(os.environ['RSEED'])
    molarity = float(os.environ['MOLE'])
except:
    job_id = 42
    molarity = 1.0
    print('parameter not found in environment, using default!')
# constants

outputFilename = 'salt_only_'+str(job_id)+'_'+str(molarity)
blx = 12
bly = 12

radi_Na = 0.15
radi_Cl = 0.21
epsilon_dict = {"0.5": 85.7,
                "0.7": 80.3,
                "1.0": 73.5,
                "2.0": 61}

eps_salt = epsilon_dict.get(str(molarity), 73.5)
print '\ndielectric constant = ' + str(eps_salt)
ion_charge = 7.49 / np.sqrt(eps_salt)

print "\nsystem unit charge = " + str(ion_charge)

N_A = 6.022 / 10.0  # Avogadro's Constant reduced
vol = blx ** 3
num_ion = int(vol * molarity * N_A)  # positive or negative, only one copy!

nve_time = 2e3  # simulation parameters

timestep = 0.001
num_particles = 2 * num_ion
print("number of particles: " + str(num_particles))

context.initialize()

snapshot = data.make_snapshot(N=num_particles, particle_types=['C', 'D'],
                              box=data.boxdim(Lx=blx, Ly=bly, Lz=blx))
snapshot.box = data.boxdim(Lx=blx, Ly=bly, Lz=blx, xy=0.0, xz=0.0, yz=0.0)  # elongated in x direction
system = init.read_snapshot(snapshot)

z_unit_vector = np.array([0, 0, 1])

idx = 0  # start from the 2nd particle, which is the first ion
delta_r = 0.6
list_ions = []
overlapCount = 0
ionCount = 0
clCount = 0
d2 = radi_Cl ** 2
print 'Add ions'
while True:
    overlap = False
    x = blx * 0.5 * (2.0 * np.random.random() - 1.0)
    y = bly * 0.5 * (2.0 * np.random.random() - 1.0)
    z = blx * 0.5 * (2.0 * np.random.random() - 1.0)

    if len(list_ions) > 0:  # check if overlap with other ions

        for pos_ions in list_ions:
            x0 = pos_ions[0] - x
            y0 = pos_ions[1] - y
            z0 = pos_ions[2] - z

            if x0 * x0 + y0 * y0 + z0 * z0 < d2:
                overlap = True
                break
            else:
                overlap = False
                # print 'ion' + str(idx)

    # add particle
    if not overlap:
        list_ions.append([x, y, z])
        snapshot.particles.position[idx] = (x, y, z)

        # assign charge  # if ion size were different, this has to be moved before comparing for judgement
        if idx < num_ion:
            snapshot.particles.charge[idx] = ion_charge
            snapshot.particles.typeid[idx] = 0  # type C
            snapshot.particles.diameter[idx] = 2 * radi_Cl
            snapshot.particles.velocity[idx] = np.random.normal(size=3)
            ionCount += 1
        elif idx < 2 * num_ion:
            snapshot.particles.charge[idx] = -ion_charge  # anions and additional ions added
            snapshot.particles.typeid[idx] = 1  # type D
            snapshot.particles.diameter[idx] = 2 * radi_Cl
            snapshot.particles.velocity[idx] = np.random.normal(size=3)
            snapshot.particles.mass[idx] = 35 / 23.5
            clCount += 1

        idx += 1
    if idx == num_particles:
        print 'ions added, overlap = ' + str(overlapCount)
        break

system.restore_snapshot(snapshot)
print 'Na' + str(ionCount)
print 'Cl' + str(clCount)

xml = hdepr.dump.xml(group=group.all(), filename=(outputFilename+'_init.xml'), vis=True)
#dump.gsd(HELIX.filename + "-initial.gsd", group=group.all(), overwrite=True, period=None)

# =================== Force Field ================================ #
sig = 1.0
epsi = 1.0

rcut2 = 2**(1/6.0)
potentialFilename = {"0.5": '05',
                     "0.7": '07',
                     "1.0": '1',
                     "2.0": '1'}


nl = nlist.cell()
table = pair.table(width=650, nlist=nl)
table.set_from_file('C', 'C', filename=('potn_NaNa-'+potentialFilename.get(str(molarity), '1')+'m.dat'))
table.set_from_file('C', 'D', filename=('potn_NaCl-'+potentialFilename.get(str(molarity), '1')+'m.dat'))
table.set_from_file('D', 'D', filename=('potn_ClCl-'+potentialFilename.get(str(molarity), '1')+'m.dat'))

typeC = group.type(name='c-particles', type='C')
typeD = group.type(name='d-particles', type='D')
typeIons = group.union(name='ions', a=typeC, b=typeD)


charged = group.charged()  # pppm section
nl = nlist.cell()
pppm = charge.pppm(group=charged, nlist=nl)
pppm.set_params(Nx=64, Ny=64, Nz=64, order=4, rcut=2.0)

integrate.mode_standard(dt=timestep)
Qlog = ['temperature', 'potential_energy', 'pair_table_energy']
logger = analyze.log(quantities=Qlog, period=2e2, filename="test{}.log".format(job_id), overwrite=True)

integrator = integrate.nve(typeIons, limit=0.01)
zero = update.zero_momentum(period=100)
# dcd = dump.dcd(filename="traj{}.dcd".format(job_id), period=5e4, overwrite=True)
dumper = dump.gsd(outputFilename + ".gsd",
                  period=1e3,
                  group=group.all(),
                  overwrite=True)
run(1000)
integrator.disable()
zero.disable()
integrate.mode_standard(dt=0.0005)
integrator = integrate.langevin(group=typeIons, kT=0.8, seed=job_id, dscale=0.2)
# integrator.set_gamma('C', gamma=0.05)
# integrator.set_gamma('D', gamma=0.05)
run(10000)
integrator.disable()

integrator = integrate.npt(group=typeIons, kT=0.8, tau=1.0, P=100.0, tauP=1.0)
run(1e6)

