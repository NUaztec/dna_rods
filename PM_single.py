from __future__ import division
import os
import sys

sys.path.append('/projects/b1030/hoomd/hoomd-2.3.0/')
from hoomd import *
from hoomd.md import *
from hoomd import deprecated as hdepr
from Potential import soft_repulsive
from Potential import zerof
import numpy as np

charge1 = 20

try:
    job_id = int(os.environ['RSEED'])
    molarity = float(os.environ['MOLE'])
except:
    job_id = 42
    molarity = 0.5
    print('parameter not found in environment, using default!')
# constants

HELIX = type('HELIX', (object,), {})()
HELIX.pitch = 0.5
HELIX.filename = 'single_neg_mole' + str(molarity) + '_PM_' + str(job_id)
blz = HELIX.pitch * charge1

HELIX.diameter = 2.0
blx = max(25 / molarity ** (1 / 3.0), 25)
bly = max(25 / molarity ** (1 / 3.0), 25)

radi_Na = 0.15
radi_Cl = 0.21
epsilon_dict = {"0.3": 92.2,
                "0.5": 85.7,
                "0.7": 80.3,
                "1.0": 73.5,
                "2.0": 60.1,
                "3.0": 48.85454,
                "4.0": 37.72745}

eps_salt = epsilon_dict[str(molarity)]
print
'\ndielectric constant = ' + str(eps_salt)
ion_charge = 7.49 / np.sqrt(eps_salt)
n_desociate = 3  # DNA charge per half nanometer
print
'charge'
print
n_desociate

print
"\nsystem unit charge = " + str(ion_charge)

N_A = 6.022 / 10.0  # Avogadro's Constant reduced
vol = blx * bly * blz - np.pi * (HELIX.diameter ** 2 / 4.0) * blz
num_ion = int(vol * molarity * N_A)  # positive or negative, only one copy!
num_counterion = int(charge1 * n_desociate)

nve_time = 2e3  # simulation parameters
total_time = 2e7
nvt_time = 4e5
n_files = int(total_time / nvt_time)
timestep = 0.001
num_particles = 2 * num_ion + num_counterion + charge1
print("number of particles: " + str(num_particles))

context.initialize()

snapshot = data.make_snapshot(N=num_particles, particle_types=['A', 'B', 'C', 'D'],
                              box=data.boxdim(Lx=blx, Ly=bly, Lz=blz))
snapshot.box = data.boxdim(Lx=blx, Ly=bly, Lz=blz, xy=0.0, xz=0.0, yz=0.0)  # elongated in x direction
system = init.read_snapshot(snapshot)

typeid_array = np.array([0, 1])
z_unit_vector = np.array([0, 0, 1])

for j in range(1):
    for i in range(charge1):  # initialize two chains of DNA beads
        snapshot.particles.position[i + j * charge1] = (
                                                               i * HELIX.pitch - blz / 2.0) * z_unit_vector
        snapshot.particles.velocity[i + j * charge1] = (0, 0, 0)
        snapshot.particles.charge[i + j * charge1] = -1 * n_desociate * ion_charge
        snapshot.particles.typeid[i + j * charge1] = 0
        snapshot.particles.diameter[i + j * charge1] = HELIX.diameter

idx = charge1  # start from the 2nd particle, which is the first ion
delta_r = 0.6
list_ions = []
overlapCount = 0
ionCount = 0
clCount = 0
anionCount = 0
d2 = radi_Cl ** 2
n_dna = charge1
forbiddenR1 = np.square(0.5 * HELIX.diameter + radi_Cl + delta_r)
print
'Add ions'
while True:
    overlap = False
    x = blx * 0.5 * (2.0 * np.random.random() - 1.0)
    y = bly * 0.5 * (2.0 * np.random.random() - 1.0)
    z = blz * 0.5 * (2.0 * np.random.random() - 1.0)
    dist_0 = np.square(x) + np.square(y)

    if dist_0 < forbiddenR1:
        overlap = True
        overlapCount += 1
        continue
    elif len(list_ions) > 0:  # check if overlap with other ions

        for pos_ions in list_ions:
            x0 = pos_ions[0] - x
            y0 = pos_ions[1] - y
            z0 = pos_ions[2] - z

            if x0 * x0 + y0 * y0 + z0 * z0 < d2:
                overlap = True
                break
            else:
                overlap = False
                # print 'ion' + str(idx)

    # add particle
    if not overlap:
        list_ions.append([x, y, z])
        snapshot.particles.position[idx] = (x, y, z)

        # assign charge  # if ion size were different, this has to be moved before comparing for judgement
        if idx < num_ion + n_dna:
            snapshot.particles.charge[idx] = ion_charge
            snapshot.particles.typeid[idx] = 2  # type C
            snapshot.particles.diameter[idx] = 2 * radi_Na
            snapshot.particles.velocity[idx] = np.random.normal(size=3)
            ionCount += 1
        elif idx < 2 * num_ion + n_dna:
            snapshot.particles.charge[idx] = -ion_charge  # anions and additional ions added
            snapshot.particles.typeid[idx] = 3  # type D
            snapshot.particles.diameter[idx] = 2 * radi_Cl
            snapshot.particles.velocity[idx] = np.random.normal(size=3)
            snapshot.particles.mass[idx] = 35 / 23.5
            clCount += 1
        else:
            snapshot.particles.charge[idx] = ion_charge  # anions and additional ions added
            snapshot.particles.typeid[idx] = 2  # type D
            snapshot.particles.diameter[idx] = 2 * radi_Na
            snapshot.particles.velocity[idx] = np.random.normal(size=3)
            snapshot.particles.mass[idx] = 1  # mass is different
            anionCount += 1

        idx += 1
    if idx == num_particles:
        print
        'ions added, overlap = ' + str(overlapCount)
        break

system.restore_snapshot(snapshot)
print
'Na' + str(ionCount)
print
'Cl' + str(clCount)
print
'Anion' + str(anionCount)
print
'counterion' + str(num_counterion)

xml = hdepr.dump.xml(group=group.all(), filename=HELIX.filename + '.xml', vis=True)
# dump.gsd(HELIX.filename + "-initial.gsd", group=group.all(), overwrite=True, period=None)

# =================== Force Field ================================ #
sig = 1.0
epsi = 1.0

delta_Na = radi_Na + HELIX.diameter / 2.0
delta_Cl = radi_Cl + HELIX.diameter / 2.0
HELIX.radius = HELIX.diameter / 2.0

rcut2 = 2 ** (1 / 6.0)
potentialFilename = {"0.3": '03',
                     "0.5": '05',
                     "0.7": '07',
                     "1.0": '1',
                     "2.0": '2',
                     "3.0": '2',
                     "4.0": '2'}

nl = nlist.cell()
table = pair.table(width=650, nlist=nl)
table.pair_coeff.set(['A', 'B'], ['A', 'B'], func=zerof, rmin=1, rmax=2, coeff=dict(epsilon=0, sigma=1.0))
table.pair_coeff.set(['A', 'B'], 'C', func=soft_repulsive, rmin=0.01, rmax=2,
                     coeff=dict(epsilon=epsi, sigma=(HELIX.radius+radi_Na)))
table.pair_coeff.set(['A', 'B'], 'D', func=soft_repulsive, rmin=0.01, rmax=2,
                     coeff=dict(epsilon=epsi, sigma=(HELIX.radius+radi_Cl)))
table.pair_coeff.set(['C', 'D'], ['C', 'D'], func=soft_repulsive, rmin=0.01, rmax=2, coeff=dict(epsilon=epsi, sigma=0.4))

typeA = group.type(name='a-particles', type='A')
typeB = group.type(name='b-particles', type='B')
typeC = group.type(name='c-particles', type='C')
typeD = group.type(name='d-particles', type='D')
typeIons = group.union(name='ions', a=typeC, b=typeD)
typeNPs = group.union(name='nps', a=typeA, b=typeB)

charged = group.charged()  # pppm section
nl = nlist.cell()
pppm = charge.pppm(group=charged, nlist=nl)
pppm.set_params(Nx=64, Ny=64, Nz=64, order=4, rcut=2.0)

integrate.mode_standard(dt=timestep)
Qlog = ['temperature', 'potential_energy', 'pair_table_energy']
logger = analyze.log(quantities=Qlog, period=2e2, filename=(HELIX.filename + ".log"), overwrite=True)

integrator = integrate.nve(typeIons, limit=0.01)
zero = update.zero_momentum(period=100)
# dcd = dump.dcd(filename="traj{}.dcd".format(job_id), period=5e4, overwrite=True)
run(nve_time)
integrator.disable()
zero.disable()
integrate.mode_standard(dt=timestep)
integrator = integrate.langevin(group=typeIons, kT=1.0, seed=job_id, dscale=0.2)
# integrator.set_gamma('C', gamma=0.05)
# integrator.set_gamma('D', gamma=0.05)
run(1e6)
for j in range(n_files):
    dumper = dump.dcd(filename=(HELIX.filename + "_{}.dcd".format(j)),
                      period=1e3)
    #  group=group.all(),

    run(nvt_time)
    dumper.disable()
logger.disable()

integrator.disable()

