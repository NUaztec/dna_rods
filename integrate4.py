#! /usr/bin/env python
import argparse
from subprocess import *
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import special
from scipy.interpolate import UnivariateSpline
from scipy.optimize import curve_fit


def shift_lj(r, rmin, rmax, epsilon, sigma, delta, shift):
    sig_r_delta6 = (sigma / (r - delta)) ** 6
    potential = 4 * epsilon * (sig_r_delta6 ** 2 - sig_r_delta6) - shift
    f = 4 * epsilon / r * (12 * sig_r_delta6 ** 2 - 6 * sig_r_delta6)  # only true for sigma = 1
    return (potential, f)


# constants
diameter = 3
ion_radius = 0.2
sig = 1.0
cutoff = 2**(1/6.0)*sig
path = './results/'
height = 12.0

#filename = HELIX.filename
#parser = argparse.ArgumentParser()
#parser.add_argument("fn1", type=str)
#parser.add_argument("fn2", type=str)
#parser.add_argument("fn3", type=str)
mole = '2.0'
charge = '0.0'
filenames = ['d_wall_bead0.6_r3.5mole{}_charge{}_IBI_11_npForce.dat'.format(mole, charge),
             'd_wall_bead0.6_r3.5mole{}_charge{}_IBI_12_npForce.dat'.format(mole, charge),
             'd_wall_bead0.6_r3.5mole{}_charge{}_IBI_13_npForce.dat'.format(mole, charge),
             'd_wall_bead0.6_r3.5mole{}_charge{}_IBI_17_npForce.dat'.format(mole, charge),
             'd_wall_bead0.6_r3.5mole{}_charge{}_IBI_18_npForce.dat'.format(mole, charge),
             'd_wall_bead0.6_r3.5mole{}_charge{}_IBI_19_npForce.dat'.format(mole, charge)]
#filenames = [fn1, fn2, fn3]
# Read files from MD Simulation ################################
data1 = np.genfromtxt(path+filenames[0], delimiter='\t')
l_data = len(data1)
dist = data1[:, 0]


 # strings of names to read in
num_files = len(filenames)
data = np.zeros((num_files, l_data, 2))
force = np.zeros(l_data)

sigForce = np.zeros(l_data)
i = 0
for filename in filenames:
    temp = np.genfromtxt(path+filename, delimiter='\t')
    data[i, :, 0] = temp[:, 1]
    data[i, :, 1] = temp[:, 2]
    force += (temp[:, 2] - temp[:, 1])
    i += 1

force = force / 2.0 / num_files

for i in range(num_files):
    sigForce += np.square(data[i, :, 1] - force)
sigForce /= num_files
sigForce = np.sqrt(sigForce)
print('sigForce=')
print(sigForce)

# discrete numerical integration ##################################
potential = np.zeros(l_data-1)
cumulated_err = np.zeros(l_data-1)
#potential[0] = 0
x_arr = np.zeros(l_data-1)
dx = data1[2, 0] - data1[1, 0]

x_arr[0] = 0.5 * (data1[0, 0] + data1[1, 0])
for i in range(l_data-2):
    x_arr[i+1] = 0.5 * (data1[i+1, 0]+data1[i+2, 0])
    #rr = fine_step*i + diameter
    #force_1 = spl_force(rr)
    #force_2 = spl_force(rr+0.01)
    potential[i+1] = potential[i] - 0.5 * dx * (force[i] + force[i+1])
    cumulated_err[l_data - i - 3] = cumulated_err[l_data - i - 2] + 0.5*dx*sigForce[l_data - i - 1]/np.sqrt(i+1)

shift = potential[l_data-2]  # comment for calculating from far to close
potential -= shift
potential = potential *10.01 / height 
x_arr_col = np.reshape(x_arr, (-1, 1))
potential_col = np.reshape(potential, (-1, 1))
potentials = np.concatenate((x_arr_col, potential_col), axis=1)
fig2, ax = plt.subplots()
ax.plot(x_arr, potential)
ax.fill_between(x_arr, potential+cumulated_err, potential-cumulated_err, facecolor='y', alpha=0.5)
#ax.plot(x_arr, potential+cumulated_err)
#ax.plot(x_arr, potential-cumulated_err)
ax.set_ylabel('potential of mean force (kT/10nm)')
ax.set_xlabel('distance (nm)')
plt.savefig(filenames[0]+'.png')
plt.show()

x_col = np.reshape(x_arr, (-1, 1))
V_col = np.reshape(potential, (-1, 1))
V_Full_out = np.concatenate((x_col, V_col), axis=1)

np.savetxt(path+filenames[1]+'potential.dat', V_Full_out)
# plot potential and spline fit of potential


'''

F = spl_force(distance)

distance_col = np.reshape(distance, (-1, 1))
V_col = np.reshape(V_full, (-1, 1))
F_col = np.reshape(F, (-1, 1))
spherePotential = np.concatenate((distance_col, V_col, F_col), axis=1)
np.savetxt('spherePot.dat', spherePotential, delimiter=' ')

potential_col = np.reshape(potential, (-1, 1))
potentials = np.concatenate((distance_col, potential_col), axis=1)

np.savetxt('potentials.dat', potentials)
plt.plot(distance, V_full)
plt.ylabel('potential (kT)')
plt.ylim(-10, 5)
plt.xlabel('distance/nm')
plt.show()

'''
