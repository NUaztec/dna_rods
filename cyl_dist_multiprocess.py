import MDAnalysis as mda
import matplotlib.pyplot as plt
import numpy as np
import multiprocessing
import gc
import time
path = './cyl_analysis/'
fn = 'single_neg_mole0.3_IBI_1'
atom_type = 'C'
binstep = 0.1
n_bins = int(10/binstep)-1
def single_hist(fn2):
    u = mda.Universe(path+fn + '.xml', path+fn2)
    num_atoms = len(u.atoms) / 2
    arr = np.arange(0, 10, binstep)
    bin_centers = np.ones(n_bins)
    for i in range(n_bins):
        bin_centers[i] = (arr[i]+arr[i+1])/2.0
    step = 0
    bin_count = np.zeros(n_bins)
    for ts in u.trajectory:
            Cl = u.select_atoms('type '+atom_type)
            #r2_Cl = np.zeros(Cl.n_atoms)
            for i in range(Cl.n_atoms):
                r = np.sqrt(Cl.positions[i][0] ** 2 + Cl.positions[i][1] ** 2)
                idx = int(r/binstep)
                if idx < n_bins:
                    bin_count[idx] += 1

        #print('bin_count = ')
        #print(len(bin_count))

            step += 1
            if step % 40 == 0:
                print(bin_count)
    bin_count = bin_count/num_atoms
    np.savetxt(path+'hist_'+fn2 + atom_type+'.txt', bin_count)


def dist_analysis(filenames):
    with multiprocessing.Pool() as pool:
        pool.map(single_hist, filenames)


if __name__ == "__main__":

    for i in range(1, 24, 7):
        filenames = [fn + '_' + str(j)+'.dcd' for j in range(i, i+7, 1)]
        print(filenames)
        dist_analysis(filenames)
        
    '''atom_type='D'
    print('starting second species...\n')
    for i in range(1, 24, 7):
        filenames = [fn + '_' + str(j)+'.dcd' for j in range(i, i+7, 1)]
        print(filenames)
        dist_analysis(filenames)
'''


#for i in range(Na.n_atoms):
#    r_Na[i] = np.sqrt(Na.positions[i][0] ** 2 + Na.positions[i][1] **2)
