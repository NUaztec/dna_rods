import numpy as np
from cyl_dist_multiprocess import n_bins, path
import matplotlib.pyplot as plt
fn='hist_single_neg_mole0.3_IBI_1_'
name = 'C'
input_fns = [path+fn+str(j)+'.dcd'+name+'.txt' for j in range(1, 20, 1)]

x = np.linspace(0, 10, n_bins)
total_N = np.zeros(n_bins)
for i in range(len(input_fns)):
    data = np.genfromtxt(input_fns[i])
    #print(data)
    total_N += data

plt.figure()
print(total_N)
print(x)
total_N = total_N/x
plt.plot(x, total_N)
plt.show()
writedata = np.zeros((n_bins, 2))
writedata[:, 0] = x
writedata[:, 1] = total_N
np.savetxt('total'+fn+'dist_'+name+'.dat', writedata)

name = 'D'
input_fns = [path+fn+str(j)+'.dcd'+name+'.txt' for j in range(1, 20, 1)]

x = np.linspace(0, 10, n_bins)
total_N = np.zeros(n_bins)
for i in range(len(input_fns)):
    data = np.genfromtxt(input_fns[i])
    #print(data)
    total_N += data

plt.figure()
print(total_N)
print(x)
total_N = total_N/x
plt.plot(x, total_N)
plt.show()
writedata = np.zeros((n_bins, 2))
writedata[:, 0] = x
writedata[:, 1] = total_N
np.savetxt('total'+fn+'dist_'+name+'.dat', writedata)
