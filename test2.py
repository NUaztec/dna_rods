import numpy as np
path = './cyl_analysis/'
fn = 'mole1.0_IBI_2'

if __name__ == "__main__":
    filenames = [fn + '_' + str(j)+'.dcd' for j in range(0, 5, 1)]
    print(filenames)
    atom_type='C'
    total_N = np.array([1,2,3])
    np.savetxt('hist_'+fn + atom_type+'_0-5.txt', total_N)
