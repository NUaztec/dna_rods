import MDAnalysis as mda
import matplotlib.pyplot as plt
import numpy as np

path = './cyl_analysis/'
fn = 'mole1.0_IBI_1.xml'
fn2 = 'sliced1.dcd'

binstep = 0.1
n_bins = int(8/binstep)

print('n_bin='+str(n_bins))
if __name__ == "__main__":
    u=mda.Universe(path+fn, path+fn2)

    total_N = np.zeros(n_bins)
    arr = np.arange(2, 10, binstep)
    bins = np.array([a ** 2 for a in arr])
    print(bins)
    Cl = u.select_atoms('type C')
    r2_Cl = np.zeros(Cl.n_atoms)
    for i in range(Cl.n_atoms):
        r2_Cl[i] = (Cl.positions[i][0] ** 2 + Cl.positions[i][1] ** 2)

    binned_values = np.digitize(r2_Cl, bins)
    print(binned_values)
    '''    
    step = 0
    for ts in u.trajectory:
        Cl = u.select_atoms('type D')
        r_Cl = np.zeros(Cl.n_atoms)
        for i in range(Cl.n_atoms):
            r_Cl[i] = np.sqrt(Cl.positions[i][0] ** 2 + Cl.positions[i][1] ** 2)

        n, bins, patches = plt.hist(r_Cl, bins=n_bins, range=(2, 9))

        total_N += n / bin_centers
        step += 1
        if step%20 ==0:
            print(total_N[2])
    '''



#for i in range(Na.n_atoms):
#    r_Na[i] = np.sqrt(Na.positions[i][0] ** 2 + Na.positions[i][1] **2)
