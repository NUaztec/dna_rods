import MDAnalysis as mda
import matplotlib.pyplot as plt
import numpy as np
import multiprocessing
import gc
import time

path = '/home/yaohua/Downloads/newQuest_b1021/DNA/24bp_AA2CG_stiff/gromacs/'
fn = 'npt'
atom_type = 'NA'
binstep = 0.5
plotRange =100
n_bins = int(plotRange / binstep) - 1


def single_hist(fn2):
    u = mda.Universe(path + fn + '.gro', path + fn2)
    num_atoms = len(u.atoms) / 2
    # calculate the center
    phos_bone = u.select_atoms('name P')
    num_phos = len(phos_bone.atoms)
    mean_xy = np.array([0, 0, 0])
    for i in range(phos_bone.n_atoms):
        #print(phos_bone.positions[i])
        mean_xy = mean_xy + np.array(phos_bone.positions[i])

    x0 = mean_xy[0]/phos_bone.n_atoms
    y0 = mean_xy[1]/phos_bone.n_atoms
    print(x0)
    arr = np.arange(0, plotRange, binstep)
    sq_bins = arr**2
    bin_centers = np.ones(n_bins)
    for i in range(n_bins):
        bin_centers[i] = (arr[i] + arr[i + 1]) / 2.0
    step = 0
    total_arr = 0
    for ts in u.trajectory:
        ion = u.select_atoms('name ' + atom_type)
        # r2_ion = np.zeros(ion.n_atoms)
        rr = (ion.positions.transpose()[0]-np.array([x0]*ion.n_atoms))**2 + \
             (ion.positions.transpose()[1]-np.array([y0]*ion.n_atoms))**2
        index_arr = np.digitize(rr, sq_bins)
        # print('bin_count = ')
        # print(len(bin_count))
        total_arr += index_arr
        step += 1
        if step % 5 == 0:
            print(index_arr)
    np.savetxt('hist_' + fn2 + atom_type + '.txt', total_arr)


def dist_analysis(filenames):
    with multiprocessing.Pool() as pool:
        pool.map(single_hist, filenames)


if __name__ == "__main__":

    filename = "npt.xtc"
    single_hist(filename)

    '''atom_type = 'D'
    print('starting second species...\n')
    for i in range(1, 24, 7):
        filenames = [fn + '_' + str(j) + '.dcd' for j in range(i, i + 7, 1)]
        print(filenames)
        dist_analysis(filenames)
'''
# for i in range(Na.n_atoms):
#    r_Na[i] = np.sqrt(Na.positions[i][0] ** 2 + Na.positions[i][1] **2)
