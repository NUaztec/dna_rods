from __future__ import division
import numpy as np
import math


class Cylinder:
    """Cylindrical model for polymers and micelles"""

    def __init__(self, radius, height, center, beadsize, charge=0):
        self.beadsize = beadsize
        self.spacing = 0.5
        self.radius = radius
        self.charge = charge
        ring_len = int(math.ceil(2 * np.pi * radius / self.spacing))
        num_stacks = int(math.ceil(height / (self.spacing*0.866)))
        base1 = np.array([1, 0, 0])
        base2 = np.array([0, 1, 0])
        base3 = np.array([0, 0, 1])
        true_height = height / num_stacks
        self.center = np.array(center)
        self.positions = []
        for i in range(num_stacks):
            for j in range(ring_len):
                theta = (j + 0.25*(-1)**i) * 2 * np.pi / ring_len
                newbead = radius * (math.cos(theta) * base1 + math.sin(theta) * base2) + (i * true_height-0.5*height) * base3 + \
                          self.center
                self.positions.append(newbead)

        self.num_beads = len(self.positions)
