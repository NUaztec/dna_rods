from __future__ import division
import os
import sys

sys.path.append('/projects/b1030/hoomd/hoomd-2.5.1/')
from hoomd import *
from hoomd.md import *
from hoomd import deprecated as hdepr
from Potential import soft_repulsive
from Potential import zerof
from Cylinder import Cylinder
from Solution import Solution
from SmallChains import SmallChains
import numpy as np

job_id = int(os.environ['RSEED'])
molarity = float(os.environ['MOLE'])
n_dissociate = float(os.environ['Charge'])
'''
try:
    job_id = int(os.environ['RSEED'])
    molarity = float(os.environ['MOLE'])
    n_dissociate = float(os.environ['Charge'])
except:
    job_id = 42
    molarity = 1.0
    n_dissociate = 0
    print('parameter not found in environment, using default!')
    '''
# constants
path = 'results/'
num_avg = 1000
intv = 150
force_sample_num = intv * num_avg
HELIX = type('HELIX', (object,), {})()

HELIX.cyl_radius = 3.5
HELIX.height = 36
HELIX.beadsize = 0.5
HELIX.chain_len = 8  
init_pos = HELIX.cyl_radius + 0.8 * HELIX.beadsize
cylinder1 = Cylinder(HELIX.cyl_radius, HELIX.height, [-init_pos, 0, 0], HELIX.beadsize, n_dissociate)
cylinder2 = Cylinder(HELIX.cyl_radius, HELIX.height, [init_pos, 0, 0], HELIX.beadsize, n_dissociate)
charge1 = cylinder1.num_beads
HELIX.filename = 'd_wall_polymer_n'+str(HELIX.chain_len) + '_r'+str(HELIX.cyl_radius)+'mole' + str(molarity) + \
                 '_' + str(job_id)

def cback_fx0(tstep):
    f_sum = 0
    for ii in range(charge1):
        p = system.particles[ii]
        f_sum += p.net_force[0]
    return f_sum


def cback_fx1(tstep):
    f_sum = 0
    for ii in range(charge1):
        p = system.particles[ii + charge1]
        f_sum += p.net_force[0]
    return f_sum


def cback_dist(tstep):
    r = system.particles[charge1].position[0] - system.particles[0].position[0]
    return r


def cback_j(tstep):
    return j_slice

delta_r = 5.0  # safe distance from box edges
bly = HELIX.height + 2 * HELIX.cyl_radius + delta_r
blz = HELIX.height
blx = 4 * HELIX.cyl_radius + blz + 4 * delta_r

final_pos = init_pos + delta_r

stepsize = 0.05
radi_Na = 0.15
radi_Cl = 0.21

eps_salt = 92
ion_charge = 7.49 / np.sqrt(eps_salt)  # system unit charge (per ion)

positions = np.arange(init_pos, final_pos + 0.00001, stepsize)

N_A = 6.022 / 10.0  # Avogadro's Constant reduced
vol = blx * bly * blz - 2 * np.pi * (HELIX.cyl_radius**2) * blz
num_chains = int(vol * molarity * N_A)
linear_bead_density = (molarity * N_A)**(1/3.0)
bead_spacing = 1 / linear_bead_density
chain = SmallChains(HELIX.chain_len, 0.5, (0, 0, 0))
sol = Solution(cylinder1, cylinder2, chain, bead_spacing, blx, bly, blz)

num_slices = len(positions)

nvt_time = 1e5
timestep = 0.002
num_particles = sol.solution_n_beads + 2 * charge1

context.initialize()

snapshot = data.make_snapshot(N=num_particles, particle_types=['A', 'B'], bond_types=[sol.b_types], box=data.boxdim(
    Lx=blx, Ly=bly, Lz=blz))


z_unit_vector = np.array([0, 0, 1])

for i in range(cylinder1.num_beads):
    snapshot.particles.position[i] = cylinder1.positions[i]
    snapshot.particles.velocity[i] = (0, 0, 0)
    snapshot.particles.charge[i] = -1 * n_dissociate * ion_charge
    snapshot.particles.typeid[i] = 0
    snapshot.particles.diameter[i] = cylinder1.beadsize

for i in range(cylinder2.num_beads):
    snapshot.particles.position[i+charge1] = cylinder2.positions[i]
    snapshot.particles.velocity[i+charge1] = (0, 0, 0)
    snapshot.particles.charge[i+charge1] = -1 * n_dissociate * ion_charge
    snapshot.particles.typeid[i+charge1] = 0
    snapshot.particles.diameter[i+charge1] = cylinder2.beadsize

idx = 2 * charge1  # start from the 2nd particle, which is the first ion
d2 = radi_Cl ** 2
# add the constituent particles to hoomd snapshot
for pos in sol.center_positions:
    chain_i = SmallChains(HELIX.chain_len, HELIX.beadsize, pos)
    for bead_pos in chain_i.positions:
        snapshot.particles.position[idx] = bead_pos
        snapshot.particles.typeid[idx] = 1
        snapshot.particles.diameter[idx] = HELIX.beadsize
        idx += 1

# add bond information
snapshot.bonds.resize(sol.num_of_bonds)
for i in range(sol.solution_n_centers):
    for j in range(chain.n_bonds):
        snapshot.bonds.group[i*chain.n_bonds+j] = chain.bondlist[j] + (i * chain.n_beads + 2 * charge1) * np.array([1,
                                                                                                                   1])
        snapshot.bonds.typeid[i*chain.n_bonds+j] = 0


system = init.read_snapshot(snapshot)
xml = hdepr.dump.xml(group=group.all(), filename=HELIX.filename + '.xml', vis=True)
#dump.gsd(HELIX.filename + "-initial.gsd", group=group.all(), overwrite=True, period=None)

# =================== Force Field ================================ #
sig = 1.0
epsi = 1.0

delta_Na = radi_Na + HELIX.cyl_radius
delta_Cl = radi_Cl + HELIX.cyl_radius

rcut2 = 2**(1/6.0)

nl = nlist.cell()
lj = pair.lj(r_cut=HELIX.beadsize*2**(1.0/6.0), nlist=nl)
lj.pair_coeff.set('A', 'A', epsilon=1.0, sigma=HELIX.beadsize, r_cut=HELIX.beadsize*2**(1.0/6.0))
lj.pair_coeff.set('A', 'B', epsilon=1.0, sigma=HELIX.beadsize, r_cut=HELIX.beadsize*2**(1.0/6.0))
lj.pair_coeff.set('B', 'B', epsilon=1.0, sigma=HELIX.beadsize, r_cut=HELIX.beadsize*2**(1.0/6.0))

typeA = group.type(name='a-particles', type='A')
typeB = group.type(name='b-particles', type='B')

harmonic = bond.harmonic(name='harmonic')
harmonic.bond_coeff.set('harmonic', k=100.0, r0=HELIX.beadsize)


integrate.mode_standard(dt=timestep)
Qlog = ['temperature', 'potential_energy', 'pair_lj_energy']
logger = analyze.log(quantities=Qlog, period=100, filename=(HELIX.filename+".log"), overwrite=True)

integrator = integrate.nve(typeB, limit=0.05)
zero = update.zero_momentum(period=100)
# dcd = dump.dcd(filename="traj{}.dcd".format(job_id), period=5e4, overwrite=True)
dumper = dump.gsd(HELIX.filename + ".gsd",
                  period=10000,
                  group=group.all(),
                  overwrite=True)

run(3000)
integrator.disable()
zero.disable()
integrate.mode_standard(dt=timestep)
integrator = integrate.langevin(group=typeB, kT=1.0, seed=job_id, dscale=0.2)
# integrator.set_gamma('C', gamma=0.05)
# integrator.set_gamma('D', gamma=0.05)
run(nvt_time)
logger.disable()

Qlog.extend(['dist_idx', 'dist', 'av_fx0', 'av_fx1'])
logger = analyze.log(quantities=Qlog, period=intv, filename=HELIX.filename+".log", overwrite=True)
logger.register_callback('av_fx0', cback_fx0)
logger.register_callback('av_fx1', cback_fx1)
logger.register_callback('dist', cback_dist)
logger.register_callback('dist_idx', cback_j)
moves = 100
dr = stepsize / moves * np.array([1, 0, 0])

j_slice = 0
run(force_sample_num)
for j_slice in range(1, num_slices-1):
    # move nano particle
    logger.disable()
    for k in range(moves):

        for i in range(charge1):
            pos0 = np.array(system.particles[i].position)
            pos1 = np.array(system.particles[i + charge1].position)
            (x0, y0, z0) = pos0 - dr
            (x1, y1, z1) = pos1 + dr
            system.particles[i].position = (x0, y0, z0)
            system.particles[i + charge1].position = (x1, y1, z1)


        run(200)
    # move
    #  finished
    run(5e4)  # to let it equilibriate
    logger.enable()

    run(force_sample_num)

integrator.disable()

xml2 = hdepr.dump.xml(group=group.all(), filename="final{}.xml".format(job_id), vis=True)
# data process
data = np.genfromtxt(HELIX.filename+".log", delimiter='\t')
size = len(data)
points = int(size / num_avg)

j = 1
for i in range(points):
    f1 = 0
    f2 = 0
    u_wall = 0
    if j < size - 2:
        dist = data[j, 5]
        while data[j, 4] == i:
            f1 += data[j, 6]
            f2 += data[j, 7]
            j += 1
            if j == size - 1:
                break
    f1 /= num_avg
    f2 /= num_avg
    with open(path+HELIX.filename+'_npForce.dat', 'a') as f:
        f.write(str(dist) + '\t' + str(f1) + '\t' + str(f2) +'\t'+str(u_wall)+ '\n')
