import numpy as np
path = 'results/'
num_avg = 10000
molarity = 2.0
n_desociate = 0.0
job_id = 13
HELIX = type('HELIX', (object,), {})()
HELIX.filename = 'd_wall_bead0.6_r3.5mole' + str(molarity) +'_charge'+str(n_desociate)+ '_IBI_' + str(job_id)
data = np.genfromtxt(HELIX.filename+".log", delimiter='\t')
size = len(data)
points = int(size / num_avg)

j = 1
for i in range(points):
    f1 = 0
    f2 = 0
    u_wall = 0
    if j < size - 2:
        dist = data[j, 5]
        while data[j, 4] == i:
            f1 += data[j, 6]
            f2 += data[j, 7]
            j += 1
            if j == size - 1:
                break
    f1 /= num_avg
    f2 /= num_avg
    with open(path+HELIX.filename+'_npForce.dat', 'a') as f:
        f.write(str(dist) + '\t' + str(f1) + '\t' + str(f2) +'\t'+str(u_wall)+ '\n')
