from __future__ import division
import numpy as np
import math


class SmallChains:

    def __init__(self, n_beads, beadsize, center):
        self.positions = []
        self.center = np.array(center)
        self.n_beads = n_beads
        base3 = np.array([0, 0, 1])
        for i in range(n_beads):
            pos = self.center + base3 * (i - 0.5*n_beads) * beadsize
            self.positions.append(pos)

        self.bondlist = []
        for i in range(n_beads-1):
            self.bondlist.append([i, i+1])
        self.n_bonds = n_beads - 1
